# myapp

A new Flutter project.

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).


# Issues
1. Driver timeout
https://github.com/flutter/flutter/issues/17184

Integration test driver does not work on iOS simulator
The test runs on physical device, but need to manually trust the app when it is reinstalled everytime.
