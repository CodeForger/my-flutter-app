import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter/material.dart';
import 'package:myapp/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  runApp(app.MyApp());
}
