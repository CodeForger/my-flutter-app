import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:myapp/main.dart';

void main() {
  testWidgets('app renders correct title', (WidgetTester tester) async {
    await tester.pumpWidget(new MyApp());
    expect(find.text('Startup Name Generator'), findsOneWidget);
  });

  testWidgets('app renders list of names', (WidgetTester tester) async {
    await tester.pumpWidget(new MyApp());
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byIcon(Icons.favorite), findsNothing);
    expect(find.byIcon(Icons.favorite_border), findsWidgets);
  });

  testWidgets('able to add favorite name', (WidgetTester tester) async {
    await tester.pumpWidget(new MyApp());
    await tester.tap(find.byIcon(Icons.favorite_border).first);
    await tester.pump();
    expect(find.byIcon(Icons.favorite), findsOneWidget);
  });
}
